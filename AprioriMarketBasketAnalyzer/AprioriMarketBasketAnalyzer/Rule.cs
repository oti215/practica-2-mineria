﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AprioriMarketBasketAnalyzer
{
    class Rule
    {
        public Rule(string predicate, string consequent)
        {
            Predicate = predicate;
            Consequent = consequent;
            ShouldRemove = false;
            findRuleSupport();
        }

        private int ruleSupportCount, predicateSupportCount;

        public string Predicate, Consequent;
        public double RuleSupportCount { get; set; }
        public int PredicateSupportCount
        {
            get
            {
                return predicateSupportCount;
            }
            set
            {
                predicateSupportCount = value;
                Trust = RuleSupportCount / PredicateSupportCount;
                SupportPercent = PredicateSupportCount / Info.transactionBasket.Count;
            }
        }
        public double SupportPercent { get; set; }
        public double Trust{ get; set; }
        public bool ShouldRemove { get; set; }

        public void findRuleSupport()
        {
            bool predicateFound = true;
            bool consequentFound = true;
            foreach (string item in Info.transactionBasket)
            {
                foreach (char letter in Predicate)
                {
                    if (!item.Contains(letter))
                        predicateFound = false;
                }
                if (predicateFound)
                {
                    PredicateSupportCount++;
                    foreach (var letter in Consequent)
                    {
                        if (!item.Contains(letter))
                            consequentFound = false;
                    }
                    if (consequentFound)
                        RuleSupportCount++;
                    else
                        consequentFound = true;
                }
                else
                    predicateFound = true;

            }

            if (Trust < Info.minTrust)
            {
                ShouldRemove = true;
            }

        }

    }
}
