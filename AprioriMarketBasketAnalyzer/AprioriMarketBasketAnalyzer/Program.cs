﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Linq;

namespace AprioriMarketBasketAnalyzer
{
    class Program
    {
        const int X = 100, Y = 6;
        static List<string> basketTransactions = new List<string>(); //read from file
        static HashSet<char> duplicateFilter = new HashSet<char>(); //all item types, with frequencies for each
        static List<ItemSet> candidateItemSet = new List<ItemSet>();
        static int minSupport = 4;
        static double minTrust = 0.30, minSupportPercentage;
        static public List<List<ItemSet>> AllFrequents;


        static void Main(string[] args)
        {
            AllFrequents = new List<List<ItemSet>>();
            string fileName;
            
            fileName = "testset1";
            minSupportPercentage = 0.40;
            minTrust = 0.50;
            
            /*Console.Write("Introduzca el nombre de archivo:");
            fileName = Console.ReadLine();
            Console.Write("Soporte Minimo:");
            minSupportPercentage = Convert.ToDouble(Console.ReadLine());
            Console.Write("Minima Confianza:");
            minTrust = Convert.ToDouble(Console.ReadLine());
            */
            processFile(fileName);//initializes basket from file and creates list of itemsets K = 1 

            int K = 2;
            Apriori analyzer = new Apriori(Info.transactionBasket, Info.minSupportCount, Info.minTrust);
            while (candidateItemSet.Count > 1)
            {
                var f = analyzer.GetFrequentSet(candidateItemSet);
                AllFrequents.Add(f); //store frecuent itemset
                calculateSupAndRemoveUnsup();

                if(K > 1)
                    combinationsFromString(K);

                K++;
            }

            List<ItemSet> lastItemSet = AllFrequents.Last(x => x.Count > 0);
            for (int i = 0; i < lastItemSet.Count; i++)
            {
                lastItemSet.ElementAt(i).createFinalSubsets();
                lastItemSet.ElementAt(i).generateRules();
            }
          
            landing(lastItemSet);

            Console.ReadKey();
        }

        static void landing(List<ItemSet> finalSet)
        {
            Console.WriteLine("TRANSACTIONS:" + Environment.NewLine);
            foreach (string item in Info.transactionBasket)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("---------------------------------------" + Environment.NewLine);
            Console.WriteLine("WE WANT:   SUPPORT=>" + Info.minSupportPercent + "   TRUST=> " + Info.minTrust + Environment.NewLine);
            Console.WriteLine("---------------------------------------" + Environment.NewLine);

            Console.WriteLine("ALGORITHM STEPS-> ");
            showAllFrequents();

            foreach (ItemSet itemSet in finalSet)
            {
                Console.WriteLine("========================");
                Console.WriteLine("FREQUENT ITEMSET: { " + itemSet.Set + " } ");
                Console.WriteLine("      ASOCIATION RULES: ");
                Console.WriteLine();
                if (itemSet.AssociationRules.Count > 0)
                {

                    foreach (Rule item in itemSet.AssociationRules)
                    {
                        Console.Write("{ " + item.Predicate + " } => { " + item.Consequent + " }");
                        Console.WriteLine("[ Soporte => " + item.RuleSupportCount + " ] , [ Confianza => " + item.Trust + " ] ");
                    }
                }
                else
                    Console.WriteLine("***NO VALID ASOCIATION RULES WERE POSSIBLE FROM THIS ITEMSET***");
                Console.WriteLine("========================");
                Console.WriteLine();
            }

        }

        static void showAllFrequents()
        {
            int i = 1;
            foreach (List<ItemSet> itemSet in AllFrequents)
            {
                Console.WriteLine("=============================");
                Console.WriteLine("FREQUENT ITEM SETS OBTAINED FROM ITERATION: " + i);
                foreach (var item in itemSet)
                {
                    Console.WriteLine("ITEMSET: " + item.Set + "  SUPPORT:" + item.Support);
                }
                i++;
                Console.WriteLine();
            }
            Console.WriteLine("=============================");
            Console.WriteLine();
        }

        static void combinationsFromString(int size)
        {
            List<string> aux = new List<string>();
            foreach (var item in candidateItemSet)
            {
                aux.Add(item.Set);
            }
            var candidateCombinations = CombinationHelper.Combinations(aux, size);
            candidateItemSet.Clear();

            List<string> noRepeats = new List<string>();
            for (int i = 0; i < candidateCombinations.Count(); i++)
            {
                string x = string.Join(" ", candidateCombinations.ElementAt(i).ToCharArray().Distinct());
                noRepeats.Add(x);
            }

            foreach (var item in noRepeats)
            {
                candidateItemSet.Add(new ItemSet(item.Replace(" ", string.Empty)));
            }
        }

        static void calculateSupAndRemoveUnsup()
        {
       
            candidateItemSet.RemoveAll(x => x.ShouldRemove);
            //AllFrequents.Add(candidateItemSet);
        }


        static void processFile(string fileName)
        {
            Info.transactionBasket = new List<string>();

            if (!fileName.Contains(".txt"))
            {
                fileName += ".txt";
            }

            //reads file and adds corresponding 0s and 1s in array
            try
            {
                StreamReader file = new StreamReader(fileName);

                string l;
                while ((l = file.ReadLine()) != null)//read lines from file, eliminating whitespace
                {
                     Info.transactionBasket.Add(l.Replace(" ", string.Empty));
                }

                foreach (string transaction in Info.transactionBasket)
                {
                    foreach (char item in transaction)
                    {
                        duplicateFilter.Add(item);//filter out repeats
                    }
                }

                foreach (char item in duplicateFilter)
                {
                    candidateItemSet.Add(new ItemSet(item.ToString()));
                }

                Info.minSupportPercent = minSupportPercentage;
                double x = (Info.transactionBasket.Count * minSupportPercentage);
                
                Info.minSupportCount = (int)Math.Ceiling(Info.transactionBasket.Count * minSupportPercentage);
                Info.minTrust = minTrust;
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found!!");   
                return;
            }
            
        }

    }
}
