﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AprioriMarketBasketAnalyzer
{
    class Info
    {

        public static int minSupportCount { get; set; }
        public static double minSupportPercent { get; set; }
        public static double minTrust { get; set; }
        public static List<string> transactionBasket { get; set; }

    }
}
