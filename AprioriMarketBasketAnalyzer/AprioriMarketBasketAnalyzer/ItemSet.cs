﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AprioriMarketBasketAnalyzer
{
    class ItemSet
    {
        
        //hashset used to rpresent items of this set
        public ItemSet(string _set)
        {
            set = _set;
            shouldRemove = false;
            support = 0;
            trust = 0;
            Subsets = new List<Subset>();
            AssociationRules = new List<Rule>();
            SubsetsForAssociationCalc = new List<Subset>();
        }

        #region privates

        private string set;
        private List<Subset> subsets;
        private List<Rule> associationRules;
        private List<string> consequent;
        private double supportPercentage, trust;
        private int support, K;
        private bool shouldRemove;

        #endregion

        //attributes
        public List<Subset> SubsetsForAssociationCalc { get; set; }
        public string Set
        {
            get
            {
                return set;
            }
            set
            {
                set = value;
            }
        }
        public List<Subset> Subsets
        {
            get
            {
                return subsets;
            }
            set
            {
                subsets = value;
            }
        } //for regular use
        public List<Rule> AssociationRules
        {
            get
            {
                return associationRules;
            }
            set
            {
                associationRules = value;
            }
        }
        public double SupportPercentage
        {
            get
            {
                return supportPercentage;
            }
            set
            {
                supportPercentage = value;
            }
        }
        public int Support
        {
            get
            {
                return support;
            }
            set
            {
                support = value;
                SupportPercentage = Support / Info.transactionBasket.Count;
            }
        }
        public double Trust
        {
            get
            {
                return trust;
            }
            set
            {
                trust = value;
            }
        }
        public bool ShouldRemove
        {
            get
            {
                return shouldRemove;
            }
            set
            {
                shouldRemove = value;
            }
        }
        public bool SubsetCreated = false;

        //find support of current Set string
        public void calculateSupport()
        {
            int occurence = 0;
            bool setFound = true;

            foreach (string transaction in Info.transactionBasket)
            {
                foreach (char item in Set)
                {
                    if (!transaction.Contains(item))
                    {
                        setFound = false;
                    }   
                }
                if (setFound)
                    occurence++;
                else
                    setFound = true;
            }

            if (occurence < Info.minSupportCount)
                shouldRemove = true; //flag for pruning
            
            support = occurence;
            supportPercentage = (double)(occurence / Info.transactionBasket.Count);
             
        }

        //find support of all subsets generated from current Set string
        public void calculateSubsetSupport()
        {
            bool shouldRem = false;

            if (this.Subsets.Count > 0)
                for (int i = 0; i < Subsets.Count ; i++)//each created subset
                    foreach (string transaction in Info.transactionBasket)//each transaction
                    {
                        foreach (char subItem in Subsets.ElementAt(i).Set)//each item in my current subset must be in the transaction
                        {
                            if (!transaction.Contains(subItem))
                            {
                                shouldRem = true; continue;
                            }
                        }
                        if (!shouldRem)
                            Subsets.ElementAt(i).Support++;
                        else 
                            shouldRem = false;
                    }
        }

        //create all possible k-sized subsets from current Set string
        public void createSubsets(int kSize)
        {
            List<string> aux = new List<string>();

            foreach (char letter in Set)
            {
                aux.Add( letter.ToString() );
            }

            var subsetStrings =  CombinationHelper.Combinations(aux, kSize).ToList();
            List<string> noRepeats = new List<string>();
            for (int i = 0; i < subsetStrings.Count(); i++)
            {
                string x = string.Join(" ", subsetStrings.ElementAt(i).ToCharArray().Distinct());
                noRepeats.Add(x);
            }

            foreach (string item in noRepeats)
            {
                Subsets.Add(new Subset(item.Replace(" ", string.Empty)));
            }
            SubsetCreated = true;
        }

        //create all possible subsets for rule generation
        public void createFinalSubsets()
        {
           
            //each individual item in the current Set is a subset
            foreach (var item in Set)
            {
                SubsetsForAssociationCalc.Add(new Subset(item.ToString())); //individual letters
            }
            //all previously calculated subsets
            SubsetsForAssociationCalc.AddRange(Subsets); //N-1 size subsets
        }

        //create association rules from final subsets
        public void generateRules()
        {
            foreach (Subset sub in Subsets)
            {
                string predicate = sub.Set;
                string consequent = removeDupes(predicate);

                AssociationRules.Add( new Rule( predicate, consequent ) );
            }
            AssociationRules.RemoveAll(rule => rule.ShouldRemove);
        }

        //auxiliary
        private string removeDupes(string predicate)
        {
           string aux = "";
            foreach (var item in Set)
            {
                if (!predicate.Contains(item))
                {
                    aux += item;
                }
            }
            return aux;
        }

        public void calculateTrust( )
        {
            // A, B =>consequentMet E (if i find this in transactions, consequentMet++)
            // /realSupport
        }

        public void calculateLift( )
        {
            // trust( i -> j ) / p(j) 
            // p(j) = frecuencia de j

        }
    

        public override string ToString()
        {
            return "Support:" + Support.ToString() + "-- Trust:" + Trust.ToString();
        }
    }
}
