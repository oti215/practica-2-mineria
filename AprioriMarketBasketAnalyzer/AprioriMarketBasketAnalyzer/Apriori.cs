﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AprioriMarketBasketAnalyzer
{
    class Apriori
    {
        public Apriori( List<ItemSet> _originalSet, List<string> _transactions, int _minSupport, double _minTrust)
        {
            OriginalSet = _originalSet; //list of item groups 
            CandidateSet = OriginalSet;
            Transactions = _transactions;//basket
            MinSupport = _minSupport;
            MinTrust = _minTrust;

            Result = new List<ItemSet>();
            AllFrequents = new List<List<ItemSet>>();
            Step = 0;
            Lift = 0;

            GetFrequentSet(CandidateSet);
        }
        public Apriori(List<string> _transactions, int _minSupport, double _minTrust)
        {
            Transactions = _transactions;//basket
            MinSupport = _minSupport;
            MinTrust = _minTrust;

            Result = new List<ItemSet>();
            AllFrequents = new List<List<ItemSet>>();
            Step = 0;
            Lift = 0;
        }


        private List<string> Transactions;
        private int Step, MinSupport;
        private double MinTrust, Lift;
        private List<ItemSet> OriginalSet;
        private List<ItemSet> CandidateSet;
        public List<ItemSet> Result;
        public List<List<ItemSet>> AllFrequents;

        public List<ItemSet> GetFrequentSet(List<ItemSet> candidates)
        {
            for (int i = 0; i < candidates.Count; i++)
            {
                candidates.ElementAt(i).calculateSupport();
                if (candidates.ElementAt(i).Set.Length > 2)
                {
                    candidates.ElementAt(i).createSubsets(candidates.ElementAt(i).Set.Length - 1);
                    candidates.ElementAt(i).calculateSubsetSupport();
                }
            }

            List<ItemSet> freqs = (List<ItemSet>)candidates.Where(x => !x.ShouldRemove).ToList();
            AllFrequents.Add(freqs);

            return freqs;
        }

        public List<string> GetAssociationRules(string x)
        {


            return null;
        }

   



    }
}
